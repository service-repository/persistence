package repository

import (
	"regexp"
	"time"
)

type Version struct {
	ServiceId uint   `sql:",pk"`
	Number    string `sql:",pk"`
}

type VersionRepository interface {
	Insert(version *Version) error
	Update(version *Version) error
	Delete(version *Version) error
	Publish(version *Version, when time.Time)
	Deprecate(version *Version, when time.Time)
	Exists(serviceId uint, number string) bool
	GetByStatus(serviceId uint, status int) []Version
	GetByNumber(serviceId uint, pattern regexp.Regexp) []Version
	GetAll(serviceId uint) []Version
}
