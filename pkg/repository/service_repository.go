package repository

import (
	"regexp"
	"time"
)

type Service struct {
	ID       uint   `sql:",pk"`
	Name     string `sql:",notnull"`
	Versions []Version
}

type ServiceRepository interface {
	Insert(service *Service) error
	Update(service *Service) error
	Delete(service *Service) error
	Publish(service *Service, when time.Time) error
	Deprecate(service *Service, when time.Time) error
	Exists(id uint) bool
	GetById(id uint) (*Service, error)
	GetByStatus(status int) ([]Service, error)
	GetByName(pattern regexp.Regexp) ([]Service, error)
	GetAll() ([]Service, error)
}
